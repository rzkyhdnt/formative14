show databases;
create database formative14;
use formative14;

INSERT INTO brand(id, name) VALUES 
(1, "Nike"), 
(2, "Adidas"),
(3, "Bata");

select * from brand;

INSERT INTO product(id, artnumber, brand_id, description, name) VALUES
(1, "CV8121-100", 1, "Sepatu zaman now parah", "Jordan Delta 2"),
(2, "CT8527-400", 1, "Sepatu ga kalah keren dari nomor 1", "Air Jordan 4"); 

select * from product;
