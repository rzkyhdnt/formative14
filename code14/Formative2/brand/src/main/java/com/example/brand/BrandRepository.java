package com.example.brand;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BrandRepository extends CrudRepository<Brand, Integer> {
    Brand findById(int id);
    List<Brand> findAll();
    void deleteById(int id);
}
