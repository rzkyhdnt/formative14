package com.example.brand;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String artnumber;
    String name;
    String description;
    int brandId;

    public Product(){

    }

    public Product(int id, String artnumber, String name, String description, int brandId){
        super();
        this.id = id;
        this.artnumber = artnumber;
        this.name = name;
        this.description = description;
        this.brandId = brandId;
    }

    public int getId(){ return id;}
    public String getArtnumber(){ return artnumber;}
    public String getName(){ return name;}
    public String getDescription() { return description;}
    public int getBrandId() { return brandId;}
    public void setId(int id){ this.id = id;}
}
