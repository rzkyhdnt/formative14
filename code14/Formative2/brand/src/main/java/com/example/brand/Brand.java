package com.example.brand;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Brand{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String name;

    public Brand(){

    }

    public Brand(int id, String name){
        super();
        this.id = id;
        this.name = name;
    }

    public int getId(){ return id;}
    public String getName(){ return name;}
    public void setId(int id){ this.id = id;}
}
