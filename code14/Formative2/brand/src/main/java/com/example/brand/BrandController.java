package com.example.brand;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class BrandController {
    @Autowired
    private BrandRepository repo;

    @Autowired
    private ProductRepository prodrepo;

    @GetMapping("/")
    public String welcome(){
        return "<html><body>"
                + "<h1>WELCOME</h1>"
                + "</body></html>";
    }

    @GetMapping("/brand")
    public List<Brand> getAllNotes() { return repo.findAll();}

    @GetMapping("/product")
    public List<Product> getAllProducts() { return prodrepo.findAll();}

    @GetMapping("/brand/{id}")
    public Brand getBrandById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }

    @GetMapping("/product/{id}")
    public Product getProductById(@PathVariable(value = "id") int id) { return prodrepo.findById(id);}

    @PostMapping("/brand")
    @ResponseStatus(HttpStatus.CREATED)
    public Brand addBrand(@RequestBody Brand brand) { return repo.save(brand);}

    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public Product addProduct(@RequestBody Product product) { return prodrepo.save(product);}

    @DeleteMapping("/delete/{id}")
    public void deleteBrand (@PathVariable(value = "id") int id) { repo.deleteById(id);}

    @DeleteMapping("/delete/product/{id}")
    public void deleteProduct (@PathVariable(value = "id") int id) { prodrepo.deleteById(id);}

    @PutMapping("/brand/{id}")
    public ResponseEntity<Object> updateProduct(@RequestBody Brand brand, @PathVariable int id){
        Optional<Brand> brandRepo = Optional.ofNullable(repo.findById(id));

        if(!brandRepo.isPresent()){
            return ResponseEntity.notFound().build();
        }
        brand.setId(id);
        repo.save(brand);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<Object> updateProducts(@RequestBody Product product, @PathVariable int id){
        Optional<Product> productRepo = Optional.ofNullable(prodrepo.findById(id));

        if(!productRepo.isPresent()){
            return ResponseEntity.notFound().build();
        }
        product.setId(id);
        prodrepo.save(product);
        return ResponseEntity.noContent().build();
    }
}
